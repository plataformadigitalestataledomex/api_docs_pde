<h2>¿Qué es?</h2>
<p>Es la especificación de los estándares de datos para realizar la comunicación entre los sistemas de las dependencias del Estado de México y la Plataforma Digital Estatal</p>

<h2>Categorías de sistemas</h2>
<ul>
  <li>Módulo 1 (<b>S1</b>): Declaraciones Patrimoniales (Inicial, Modificación y Conclusión)</li>
  <li>Módulo 2 (<b>S2</b>): Servidores públicos que intervienen en contrataciones</li>
  <li>Módulo 3 (<b>S3</b>): Servidores públicos y particulares sancionados</li>
  <li>Módulo 6 (<b>S6</b>): Información pública de contrataciones</li>
</ul>
<h2>Validador de estructura de datos SESAEMM </h2>

<p>Te invitamos a probar **[Validador de estructura de datos SESAEMM](https://saemm.gob.mx/validadorPDE/)** que permitirá ejecutar de manera automatizada validaciones de estructura de respuesta del API para los sistemas I, II, III y VI, recibiendo retroalimentación de manera rápida. Esta herramienta se encuentra en una etapa de pilotaje, por lo que el resultado obtenido debe considerarse una prevalidación y no como un resultado final.</p>
